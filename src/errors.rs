#[derive(Debug)]
pub enum Errors {
    IOError(std::io::Error),
    NetworkError(Box<ureq::Error>),
    InvalidGenre(usize),
    InvalidConfig(toml_edit::de::Error),
    InvalidConfigType(toml_edit::ser::Error)
}
impl From<std::io::Error> for Errors {
    fn from(e: std::io::Error) -> Errors {
        Errors::IOError(e)
    }
}
impl From<ureq::Error> for Errors {
    fn from(e: ureq::Error) -> Errors {
        Errors::NetworkError(Box::new(e))
    }
}
impl From<toml_edit::de::Error> for Errors {
    fn from(e: toml_edit::de::Error) -> Errors {
        Errors::InvalidConfig(e)
    }
}
impl From<toml_edit::ser::Error> for Errors {
    fn from(value: toml_edit::ser::Error) -> Self {
        Errors::InvalidConfigType(value)
    }
}
