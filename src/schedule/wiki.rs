use std::collections::HashMap;

use serde::Deserialize;

use super::config;

#[derive(Deserialize)]
struct WikiDataPage {
    pageid: usize,
    ns: usize,
    title: String,
    index: usize
}
#[derive(Deserialize)]
struct WikiDataQuery {
    pages: HashMap<String, WikiDataPage>
}
#[derive(Deserialize)]
struct WikiData {
    batchcomplete: String,
    query: WikiDataQuery
}

fn url_encode(s: String) -> String {
    s.replace(' ', "%20")
}

pub fn find_wiki_data(conf: &config::Config, name: &str) -> Option<String> {
    // TODO urlencode name
    let body: String = ureq::get(
        &format!(
            "{}/w/api.php?action=query&origin=*&format=json&generator=search&gsrnamespace=0&gsrlimit=1&gsrsearch=\"{}\"",
            conf.features.wiki.url,
            url_encode(name.to_string())
        ))
        .call().ok()?
               .into_string().ok()?;
    if let Ok(dt) = serde_json::from_str::<WikiData>(&body) {
        let pages: Vec<&WikiDataPage> = dt.query.pages.values().collect();
        if let Some(page) = pages.first() {
            return Some(page.title.to_string());
        }
    }

    None
}
