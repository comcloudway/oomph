mod config;
mod post;
mod wiki;

use std::{
    path::PathBuf,
    fs::File, io::Read
};
use post::{
    PollOptions,
    Post
};
use chrono::{
    NaiveDateTime,
    Duration,
    Utc
};

use crate::{
    errors::Errors,
    schedule::config::Config
};

pub fn schedule(config: &PathBuf, batches: usize) -> Result<(), Errors> {
    let mut conf = config::Config::read_from_file(config)?;
    let start_index = conf.state.index;
    let start_date = NaiveDateTime::parse_from_str(
        &(conf.state.date.clone().unwrap_or(String::from(" ")) + "00:00:00"),
        "%Y-%m-%d %H:%M:%S").unwrap_or(
        Utc::now().date_naive().and_hms_opt(0, 0, 0).unwrap());

    let mut time_stamp = start_date;
    let time_segments = conf.config.time.split(':')
                                        .map(|d| d.to_string())
                                        .collect::<Vec<String>>();
    if let Some(h) = time_segments.get(0) {
        let hours = h.parse::<i64>().unwrap_or(0);
        time_stamp = time_stamp.checked_add_signed(Duration::hours(hours)).unwrap();
        if let Some(m) = time_segments.get(1) {
            let mins = m.parse::<i64>().unwrap_or(0);
            time_stamp = time_stamp.checked_add_signed(Duration::minutes(mins)).unwrap();
        }
    }

    let genres: Vec<String> = {
        let mut genre_list = File::open(&conf.config.file)?;
        let mut cont = String::new();
        genre_list.read_to_string(&mut cont)?;
        cont.lines().map(|d| d.to_string()).collect()
    };

    let mut index = start_index;

    for offset in 0..(batches * 4) {
        let genre = if let Some(g) = genres.get(index) { g } else {
            return Err(Errors::InvalidGenre(index))
        };

        if conf.features.poll.enable && (offset+1)%4==0 {
            let time_stamp = time_stamp.checked_add_signed(Duration::days(conf.features.poll.offset as i64)).unwrap();
            let schedule = time_stamp.format("%Y-%m-%dT%H:%M:%S+00:00");

            let poll = Post::Poll {
                status: format!("{}\n{}",
                                conf.features.poll.text,
                                conf.features.poll.footer),
                visibility: conf.config.post.visibility.clone(),
                scheduled_at: schedule.to_string(),
                poll: PollOptions {
                    options: genres.as_slice()[(start_index + offset - 4 + 1) .. (start_index + offset + 1)].to_vec(),
                    multiple: conf.features.poll.multi,
                    expires_in: conf.features.poll.offset * 60 * 60 * 24
                }
            };

            println!("Posting poll: {:?}", &poll);
            if let Err(e) = poll.publish(&conf) {
                backup(config, &mut conf, time_stamp, index)?;
                return Err(e);
            }
        }

        let schedule = time_stamp.format("%Y-%m-%dT%H:%M:%S+00:00");
        let banner = conf.config.post.text.replace("%s", genre);
        let mut post = Post::Genre {
            status: format!("{}\n{}",
                            banner,
                            conf.config.post.footer),
            sensitive: false,
            visibility: conf.config.post.visibility.clone(),
            scheduled_at: schedule.to_string()
        };

        if conf.features.wiki.enable {
            if let Some(title) = wiki::find_wiki_data(&conf, genre) {
                let wiki_url = format!("{}/wiki/{}", conf.features.wiki.url, title.replace(' ', "%20"));

                post = Post::GenreCW {
                    status: format!("{}\n{}",
                                    conf.features.wiki.text_with_link
                                    .replace("%s", genre)
                                    .replace("%q", &wiki_url),
                                    conf.config.post.footer),
                    sensitive: true,
                    visibility: conf.config.post.visibility.clone(),
                    scheduled_at: schedule.to_string(),
                    spoiler_text: format!("{}. {}",
                                          banner,
                                          conf.features.wiki.content_warning
                    )
                }
            }
        }

        println!("Posting genre: {:?}", &post);
        if let Err(e) = post.publish(&conf) {
            backup(config, &mut conf, time_stamp, index)?;
            return Err(e);
        }

        // next time-stamp
        time_stamp = time_stamp.checked_add_signed(Duration::days(conf.config.post.offset as i64)).unwrap();
        // next genre
        index+=1;
    }

    backup(config, &mut conf, time_stamp, index)?;

    Ok(())
}

fn backup(file: &PathBuf, conf: &mut Config, time_stamp: NaiveDateTime, index: usize) -> Result<(), Errors> {

    conf.state.date = Some(time_stamp.date().to_string());
    conf.state.index = index;

    conf.write_to_file(file)?;
    Ok(())
}
