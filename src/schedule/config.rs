use std::{
    path::PathBuf,
    fs::File,
    io::{Read,Write}
};
use serde::{Deserialize, Serialize};
use crate::Errors;

fn default_true() -> bool {
    true
}
fn default_false() -> bool {
    false
}

#[derive(Deserialize, Serialize)]
pub struct Config {
    pub auth: Auth,
    pub config: BotConf,
    pub features: Feature,
    pub state: State
}

/// Login Data
/// you can obtain the login data
/// by opening the Mastodon Preferences panel
/// and creating a new application in the Development tab
/// (it needs write access)
/// Afterwards you can open the application and copy the access token
#[derive(Deserialize, Serialize)]
pub struct Auth {
    /// the homeserver url
    /// e.g. https://botsin.space
    pub server: String,
    /// your access token
    pub token: String
}

fn default_time() -> String {
    String::from("09:00")
}
/// Bot configuration
#[derive(Deserialize, Serialize)]
pub struct BotConf {
    /// The file containing the shuffled genres
    /// (NOTE: one genre per line)
    pub file : String,
    /// at which time to publish the posts
    #[serde(default = "default_time")]
    pub time : String,
    /// Additional post config
    pub post: BotPostConf
}

fn default_post_offset() -> usize {
    2
}
fn default_post_footer() -> String {
    String::from("#music #discover")
}
fn default_post_text() -> String {
    String::from("Genre of the Day: %s")
}
/// Additional configuration for posts
#[derive(Deserialize, Serialize)]
pub struct BotPostConf {
    /// how often to post a "Genre of the day" post
    /// 2 means every other day
    /// [0] 1 [2] 3 [4] 5 [6]
    #[serde(default = "default_post_offset")]
    pub offset: usize,
    /// append a footer to the post
    #[serde(default = "default_post_footer")]
    pub footer: String,
    /// limit the post visibility to one of the following
    /// see Mastodon API doc
    #[serde(default)]
    pub visibility: PostVisibility,
    /// content warning prefix
    /// and default announcement text
    /// %s will be replaced with the genre name
    #[serde(default = "default_post_text")]
    pub text: String
}
/// Mastodon post visibility
/// see https://docs.joinmastodon.org/methods/statuses/
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum PostVisibility {
    /// visible everywhere
    /// you probably want to use this
    #[serde(rename = "public")]
    Public,
    /// not shown in feterated timelines
    #[serde(rename = "unlisted")]
    Unlisted,
    /// only shown to followers
    #[serde(rename = "private")]
    Private,
    /// only sent to people mentioned
    /// requires you to tag people
    /// (e.g. using the footer)
    #[serde(rename = "direct")]
    Direct
}
impl Default for PostVisibility {
    fn default() -> Self {
        Self::Public
    }
}

/// Feature collection
#[derive(Deserialize, Serialize)]
pub struct Feature {
    pub poll: PollFeature,
    pub wiki: WikiFeature
}

fn default_poll_footer() -> String {
    String::from("#music")
}
fn default_poll_offset() -> usize {
    1
}
fn default_poll_text() -> String {
    String::from("Over the last couple of days, you were abe to discover 4 new genres.\n Choose your favourite:")
}
/// Additional configurations for polls
/// The poll feature allows you to post a poll
/// per every 4 posts,
/// asking the community to vote for their favourite
#[derive(Deserialize, Serialize)]
pub struct PollFeature {
    /// allwos you to disable polls
    /// true to post a polls after 4 posts
    #[serde(default = "default_true")]
    pub enable: bool,
    /// how many days after the last genre-post
    /// to publish the poll
    #[serde(default = "default_poll_offset")]
    pub offset: usize,
    /// allows the user to select multiple genres
    #[serde(default = "default_false")]
    pub multi: bool,
    /// append a footer to the poll post
    #[serde(default = "default_poll_footer")]
    pub footer: String,
    /// the poll title text
    #[serde(default = "default_poll_text")]
    pub text: String
}

fn default_wiki_url() -> String {
    String::from("https://en.wikipedia.org")
}
fn default_url_warning() -> String {
    String::from("Automatically generated link!")
}
fn default_wiki_text() -> String {
    String::from("If you want to learn more about %s, this link might be helpful: %q")
}
/// Additional configurations for wikipedia integration
/// the wiki feature allows the bot to fetch available genre infos
/// from wikipedia
/// (NOTE: because of possible copyright,
/// it'll only post a link to the given wiki page)
#[derive(Deserialize, Serialize)]
pub struct WikiFeature {
    /// allow you to disable the wikipedia integration
    #[serde(default = "default_false")]
    pub enable: bool,
    /// set the wikipedia url
    /// can be used to use alternative wikipedia front-ends
    /// oomph will append additional link data
    /// (including paths and query parameters)
    /// to this link
    #[serde(default = "default_wiki_url")]
    pub url: String,
    /// Automatically generated links content warning
    #[serde(default = "default_url_warning")]
    pub content_warning: String,
    /// spoiler-content
    /// %s will be replaced with the genre name
    /// %q will be replaced with the wikipedia url
    #[serde(default = "default_wiki_text")]
    pub text_with_link: String
}

/// Used by oomph a kind of database
/// to keep track of the important values for you
#[derive(Deserialize, Serialize)]
pub struct State {
    /// The current genre in the list
    /// starting with 0
    /// NOTE: you probably don't want to change this
    /// after running oomph the first time
    pub index: usize,
    /// date of next post
    /// you probably should change this
    /// before running oomph for the first time
    /// this timestamp should either be today
    /// or in the future
    pub date: Option<String>
}

impl Config {
    /// reads the content from a file and tries parsing it as a config file
    pub fn read_from_file(file: &PathBuf) -> Result<Self, Errors> {
        let mut file = File::open(file)?;
        let mut cont = String::new();
        file.read_to_string(&mut cont)?;

        Ok(toml_edit::de::from_str(&cont).expect("invalid config"))
    }

    /// write the config to disk
    pub fn write_to_file(&self, file: &PathBuf) -> Result<(), Errors> {
        let toml = toml_edit::ser::to_string_pretty(self)?;
        let mut config_file = File::create(file)?;
        config_file.write_all(toml.as_bytes())?;

        Ok(())

    }
}
