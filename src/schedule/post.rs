use serde::Serialize;

use crate::errors::Errors;

use super::config::{
    PostVisibility,
    Config
};

#[derive(Serialize, Debug)]
pub struct PollOptions {
    /// NOTE: max-length: 4
    pub options: Vec<String>,
    pub multiple: bool,
    pub expires_in: usize
}

#[derive(Serialize, Debug)]
#[serde(untagged)]
pub enum Post {
    Genre {
        status: String,
        sensitive: bool,
        visibility: PostVisibility,
        scheduled_at: String
    },
    Poll {
        status: String,
        visibility: PostVisibility,
        scheduled_at: String,
        poll: PollOptions
    },
    GenreCW {
        status: String,
        sensitive: bool,
        visibility: PostVisibility,
        scheduled_at: String,
        spoiler_text: String
    }
}
impl Post {
    pub fn publish(&self, config: &Config) -> Result<(), Errors> {
        let _body: String = ureq::post(&format!("{}/api/v1/statuses", config.auth.server))
            .set("Authorization", &format!("Bearer {}", config.auth.token))
            .set("Content-Type", "application/json")
            .send_json(self)?
            .into_string()?;
        Ok(())
    }
}
