mod schedule;
mod shuffle;
mod errors;

use std::path::PathBuf;
use clap::Parser;

use crate::shuffle::shuffle;
use crate::schedule::schedule;
use crate::errors::Errors;


#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
enum Commands {
    /// shuffle a list of genres
    Shuffle {
        /// Input file to be shuffled
        /// (output will be written to the same file)
        #[arg(short, long)]
        file: PathBuf
    },
    /// schedule a specific amout of posts (in batches of 4)
    Schedule {
        /// Path to config.toml
        #[arg(short='c', long)]
        config: PathBuf,
        #[arg(short='n', long="count")]
        batch_count: usize,
    }
}

fn main() -> Result<(), Errors> {
    let cli = Commands::parse();

    match &cli {
        Commands::Shuffle { file } => {
            shuffle(file)
        },
        Commands::Schedule { config, batch_count } => {
            schedule(config, *batch_count)
        }
    }
}
