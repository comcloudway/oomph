use std::{
    path::PathBuf,
    fs::File,
    io::{Read, Write},
};
use rand::{
    thread_rng,
    seq::SliceRandom
};
use crate::errors::Errors;

pub fn shuffle(file: &PathBuf) -> Result<(), Errors> {
    let mut file = File::open(file)?;
    let mut cont = String::new();
    file.read_to_string(&mut cont)?;
    let mut lines = cont
        .lines()
        .map(|d| d.to_string())
        .collect::<Vec<String>>();

    lines.shuffle(&mut thread_rng());

    file.write_all(lines.join("\n").as_bytes())?;

    Ok(())
}
